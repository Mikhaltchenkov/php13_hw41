<?php
function addStatement($statement, $addition)
{
    if (!empty($statement)) {
        return $statement . ' AND ' . $addition;
    } else {
        return $addition;
    }
}

$where = "";
$whereArgs = [];

if (!empty($_GET['isbn'])) {
    $whereArgs[':isbn'] = '%'. $_GET['isbn'] . '%';
    $where = addStatement($where, " isbn like :isbn");
    $isbn = $_GET['isbn'];
} else {
    $isbn = '';
}
if (!empty($_GET['name'])) {
    $whereArgs[':name'] = '%' . $_GET['name'] . '%';
    $where = addStatement($where, " name like :name");
    $name = $_GET['name'];
} else {
    $name = '';
}
if (!empty($_GET['author'])) {
    $whereArgs[':author'] = '%' . $_GET['author'] . '%';
    $where = addStatement($where, " author like :author");
    $author = $_GET['author'];
} else {
    $author = '';
}
?>
<html>
<head>
    <title>Моя библиотека</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <h3>Моя библиотека</h3>
    <div class="page-header">
        <form action="index.php" method="get">
            <input type="text" name="isbn" placeholder="ISBN" value="<?= $isbn ?>">
            <input type="text" name="name" placeholder="Название книги"
                   value="<?= $name ?>">
            <input type="text" name="author" placeholder="Автор книги"
                   value="<?= $author ?>">
            <input type="submit" name="submit" value="Поиск">
        </form>
    </div>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Название</th>
            <th>Автор</th>
            <th>Год выхода</th>
            <th>ISBN</th>
            <th>Жанр</th>
        </tr>
        </thead>
        <tbody>

        <?php

//        $pdo = new PDO("mysql:host=localhost;dbname=netology;charset=utf8", "root", "");
        $pdo = new PDO("mysql:host=localhost;dbname=global;charset=utf8", "mikhaltchenkov", "neto1163");


        if (!empty($where)) {
            $where = ' WHERE ' . $where;
        }
        $sql = "SELECT name, author, year, isbn, genre FROM books $where";
        $sth = $pdo->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));

        $sth->execute($whereArgs);
        $result = $sth->fetchAll();
        foreach ($result as $row) {
            echo '<tr><td>' . $row['name'] . '</td><td>' . $row['author'] . '</td><td>' . $row['year'] . '</td><td nowrap="">' . $row['isbn'] . '</td><td>' . $row['genre'] . '</td></tr>';
        }
        ?>
        </tbody>
    </table>
</div>
<div id="footer">
    <div class="container"><p class="text-muted"> ©2017, Михальченков Дмитрий</p></div>
</div>
</body>
</html>

